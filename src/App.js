import React, { useContext, useState } from 'react';
import SettingsContext from './store/settings-context';
import Header from './components/Layout/Header';
import GameBoard from './components/Game/GameBoard';
import GameSettings from './components/Game/GameSettings';
import NewGameMenu from './components/Game/NewGameMenu';
import styles from './App.module.css';

const App = () => {
  const { difficulty, gamePack } = useContext(SettingsContext);
  const [settingsIsOpen, setSettingsIsOpen] = useState(false);
  const [gameFinished, setGameFinished] = useState(false);

  const showSettingsHandler = () => {
    setSettingsIsOpen(true);
  };

  const hideSettingsHandler = () => {
    setSettingsIsOpen(false);
  };

  const gameFinishedHandler = () => {
    setGameFinished(true);
  };

  const newGameHandler = () => {
    setGameFinished(false);
  };

  const gameBg = {
    backgroundImage: `url(${gamePack.background})`
  }

  return (
    <div className={`${styles.App}`} style={gameBg}>
      {settingsIsOpen && <GameSettings onHideSettings={hideSettingsHandler} />}
      <Header onShowSettings={showSettingsHandler} />
      {
        !gameFinished &&
        <div className={`${styles.game} ${styles['game--' + difficulty]}`}>
          <GameBoard onGameFinished={gameFinishedHandler} />
        </div>
      }
      {gameFinished && <NewGameMenu onNewGame={newGameHandler} />}
    </div>
  );
};

export default App;