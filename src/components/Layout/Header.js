import React from 'react';

import styles from './Header.module.css';
import gearImg from '../../assets/icons8-settings.svg';

const Header = (props) => {
  return (
    <div className={styles.header}>
      <p>Movie Memory Game</p>
      <button onClick={props.onShowSettings}>
        <img src={gearImg} alt="Settings gear" />
      </button>
    </div>
  );
};

export default Header;