import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

import styles from './Modal.module.css';

const Backdrop = (props) => {
  const escHandler = (e) => {
    if (e.key === 'Escape') props.onClose();
  };
  //TODO: fix accessibility for modal
  // eslint-disable-next-line jsx-a11y/no-static-element-interactions
  return <div
    className={styles.backdrop}
    onKeyDown={escHandler}
    onClick={props.onClose}
  />;
};

const ModalOverlay = (props) => {
  return <Fragment>
    <div className={styles.modal}>{props.children}</div>
  </Fragment>;
};

const portalElement = document.getElementById('overlays');

const Modal = (props) => {
  return (<Fragment>
    {ReactDOM.createPortal(<Backdrop onClose={props.onClose} />, portalElement)}
    {ReactDOM.createPortal(<ModalOverlay>{props.children}</ModalOverlay>,
      portalElement)}
  </Fragment>);
};

export default Modal;