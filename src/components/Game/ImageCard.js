import React, { useContext, useEffect, useState } from 'react';
import SettingsContext from '../../store/settings-context';
import styles from './ImageCard.module.css';

const ImageCard = (props) => {
  const { gamePack } = useContext(SettingsContext);
  const [isFlipped, setIsFlipped] = useState(false);
  const [isMatched, setIsMatched] = useState(false);

  const notMatched = () => {
    setTimeout(() => setIsFlipped(false), 1000);
  };

  const setMatched = () => {
    setTimeout(() => setIsMatched(true), 1000);
  };

  const reset = () => {
    setIsFlipped(false);
    setIsMatched(false);
  };

  const flip = () => {
    props.onCardFlip(
      {
        id: props.id,
        alt: props.alt,
        matched: isMatched,
        notMatched,
        setMatched,
        setIsFlipped,
      });
  };

  useEffect(() => {
    props.resetCard({ reset });
  }, [props, props.resetCard]);

  return (
    <button
      className={`${styles['image-card']} ${isFlipped &&
      styles['is-flipped']} ${isMatched && styles['is-matched']}`}
      onClick={flip}
    >
      <div className={styles['inner-card']}>
        <div className={styles['card-front']}>
          <img src={gamePack.logo} alt={gamePack.logoAlt} />
        </div>
        <div className={styles['card-back']}>
          <img src={props.src} alt={props.alt} />
        </div>
      </div>
    </button>
  );
};

export default ImageCard;