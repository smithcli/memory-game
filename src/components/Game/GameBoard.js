import React, { useContext, useEffect, useState } from 'react';
import SettingsContext from '../../store/settings-context';
import ImageCard from './ImageCard';
import shuffleArray from '../../utils/shuffleArray';
import styles from './GameBoard.module.css';

const GameBoard = ({ onGameFinished }) => {
  // Game Settings
  const { difficulty, cardPairs, gamePack } = useContext(SettingsContext);
  // Cards in play
  const [gameCards, setGameCards] = useState([]);

  // Game Logic
  let flippedCards = [];
  let numFlipped = 0;
  let numMatched = 0;
  let readyForNext = true; // Protect against spam clicking
  const cardFlippedHandler = (card) => {
    // Don't allow clicks on matched cards
    if (readyForNext && !card.matched && flippedCards.length < 1) {
      card.setIsFlipped(true);
      flippedCards.push(card);
      numFlipped += 1;
    } else if (readyForNext
      && !card.matched
      && card.id !== flippedCards[0].id) {
      card.setIsFlipped(true);
      flippedCards.push(card);
      numFlipped += 1;
    }
    // Check if match
    if (flippedCards.length === 2
      && flippedCards[0].id !== flippedCards[1].id
      && flippedCards[0].alt === flippedCards[1].alt) {
      flippedCards.forEach((card) => card.setMatched());
      flippedCards = [];
      numMatched += 1;
      //console.log(`${numMatched}/${cardPairs}`);
      // No Match
    } else if (flippedCards.length === 2) {
      readyForNext = false;
      setTimeout(() => readyForNext = true, 1000);
      flippedCards.forEach((card) => card.notMatched());
      flippedCards = [];
    }
    if (numFlipped === 1) {
      //console.log('Game Started');
    }
    if (numMatched === cardPairs) {
      //console.log('Game Complete!');
      onGameFinished();
    }
  };

  // To Reset card state when new game loads.
  const gameReset = (card) => {
    if (numFlipped === 0) {
      card.reset();
    }
  };

  // Initialize Game Cards
  useEffect(() => {
    // Different start point allows different cards to be seen on same difficulty
    const startPoint = Math.floor(
      Math.random() * (gamePack.cards.length + 1 - cardPairs),
    );

    // cardPack size is determined by difficulty
    const cardPack = gamePack.cards.filter((el, index) => {
      return index >= startPoint && index < startPoint + cardPairs;
    });

    // Add matching pairs of cards to allCards Array
    let allCards = [];
    const url = process.env.PUBLIC_URL;
    cardPack.forEach((card) => {
      allCards.push({
        key: card.id + 'A',
        id: card.id + 'A',
        alt: card.alt,
        src: `${url}/img/${gamePack.asset}/${card.image}`,
      });
      allCards.push({
        key: card.id + 'B',
        id: card.id + 'B',
        alt: card.alt,
        src: `${url}/img/${gamePack.asset}/${card.image}`,
      });
    });
    setGameCards(shuffleArray(allCards));
    //console.log('Cards Created');
  }, [cardPairs, gamePack]);

  return (
    <div className={styles[difficulty]}>
      {gameCards.map((card) => {
        return <ImageCard
          key={card.key}
          id={card.id}
          src={card.src}
          alt={card.alt}
          onCardFlip={cardFlippedHandler}
          resetCard={gameReset}
        />;
      })}
    </div>
  );
};

export default GameBoard;