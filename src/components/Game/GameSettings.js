import React, { useContext } from 'react';
import SettingsContext from '../../store/settings-context';
import Modal from '../UI/Modal';
import styles from './GameSettings.module.css';

const GameSettings = (props) => {
  const settingsCtx = useContext(SettingsContext);

  const onChangeHandler = ({ target }) => {
    if (target.name === 'difficulty') settingsCtx.setDifficulty(target.value);
  };

  return (
    <Modal onClose={props.onHideSettings}>
      <div className={styles.gameSettingsHeader}>
        <h1>Game Settings</h1>
        <button onClick={props.onHideSettings}>&#10006;</button>
      </div>
      <form className={styles.form}>
        <fieldset className={styles.fieldset}>
          <legend>Difficulty:</legend>
          <div>
            <label>
              <input
                onChange={onChangeHandler}
                type={'radio'}
                id={'easy'}
                name={'difficulty'}
                value={'easy'}
                checked={settingsCtx.difficulty === 'easy'}
              /> Easy</label>
            <label>
              <input
                onChange={onChangeHandler}
                type={'radio'}
                id={'normal'}
                name={'difficulty'}
                value={'normal'}
                checked={settingsCtx.difficulty === 'normal'}
              /> Normal</label>
            <label>
              <input
                onChange={onChangeHandler}
                type={'radio'}
                id={'hard'}
                name={'difficulty'}
                value={'hard'}
                checked={settingsCtx.difficulty === 'hard'}
              /> Hard</label>
          </div>
        </fieldset>
        <label htmlFor={'pack'}>Movie:</label>
        <select name={'pack'} id={'pack'} onChange={onChangeHandler}>
          <option value={'shrek'}>Shrek</option>
        </select>
      </form>
    </Modal>
  );
};

export default GameSettings;