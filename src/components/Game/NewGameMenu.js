import React, { Fragment, useContext } from 'react';
import SettingsContext from '../../store/settings-context';
import styles from './NewGameMenu.module.css';

const NewGameMenu = (props) => {
  const { difficulty, setDifficulty } = useContext(SettingsContext);

  const playHarderGame = () => {
    if (difficulty === 'easy') {
      setDifficulty('normal');
      props.onNewGame();
    }
    if (difficulty === 'normal') {
      setDifficulty('hard');
      props.onNewGame();
    }
  };

  const nextLevel = () => {
    if (difficulty === 'easy') {
      return <Fragment>
        <p>Too Easy?</p>
        <button onClick={playHarderGame}>Play again on Normal!</button>
      </Fragment>;
    }
    if (difficulty === 'normal') {
      return <Fragment>
        <p>Ready for a challenge?</p>
        <button onClick={playHarderGame}>Play again on Hard!</button>
      </Fragment>;
    }
  };

  return (<div className={styles.newGameMenu}>
    <div>
      <h3>Hope you enjoyed the game!</h3>
      <p>Press the button to start a new game</p>
      <button onClick={props.onNewGame}>Play Again!</button>
      {nextLevel()}
      <p>Hint: Use settings gear to try another difficulty or theme</p>
    </div>
    <footer>
      <p>Please report any bugs or place any feature
        requests <a href={'https://gitlab.com/smithcli/moviemg/issues'}>here</a>.
      </p>
      <p>
        Copyright 2022 Christopher Smith<br />
        SPDX-License-Identifier: Apache-2.0
      </p>
      <p>
        <a
          href={'https://icons8.com/icon/FtTnPlZdnUX5/settings'}
        >Settings</a> icon by <a href={'https://icons8.com'}>Icons8</a>
      </p>
      <p>
        Shrek Character Images and Logo from
        <a href={'https://shrek.fandom.com/wiki/WikiShrek'}> Shrek Wiki </a>
      </p>
      <p>
        Image of shrek's house
        from <a href={'https://pin.it/15Pm5Cq'}>Pinterest</a>
      </p>
      <p>Source Code, Full Credits and License
        on <a href={'https://gitlab.com/smithcli/moviemg'}>gitlab</a>.</p>
    </footer>
  </div>);
};

export default NewGameMenu;