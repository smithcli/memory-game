import background from './background.png';
import cards from './shrek.json';
import logo from './shrek-logo.png';

const SHREK = {
  asset: 'shrek',
  cards,
  logo,
  logoAlt: 'shrek "S" logo',
  background,
};

export default SHREK;