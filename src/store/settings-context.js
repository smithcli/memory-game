import React, { useState } from 'react';
import SHREK from '../assets/shrek';

const SettingsContext = React.createContext({
  difficulty: 'easy',
  gamePack: SHREK,
  cardPairs: 8,
  setDifficulty: () => {},
  setGamePack: () => {},
});

export const SettingsCtxProvider = (props) => {
  const [difficulty, setDifficulty] = useState('easy');
  const [gamePack, setGamePack] = useState(SHREK);
  const [cardPairs, setCardPairs] = useState(8);

  const difficultyHandler = (level) => {
    setDifficulty(level);
    // Easy = 8, Normal = 16, Hard = 24
    if (level === 'easy') setCardPairs(8);
    else if (level === 'normal') setCardPairs(16);
    else setCardPairs(24);
  };

  return <SettingsContext.Provider
    value={{
      difficulty: difficulty,
      gamePack: gamePack,
      cardPairs: cardPairs,
      setDifficulty: difficultyHandler,
      setGamePack: setGamePack,
    }}
  >{props.children}</SettingsContext.Provider>;
};

export default SettingsContext;