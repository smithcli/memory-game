import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders game title', () => {
  render(<App />);
  // noinspection JSCheckFunctionSignatures
  const title = screen.getByText(/Movie Memory Game/i);
  expect(title).toBeInTheDocument();
});
