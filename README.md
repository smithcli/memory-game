# Movie Memory Game

![Screenshot of Movie Memory Game on easy](screenshots/easy-game.png)

## Objective

Compete against time to match all the cards with the same movie character image.

## Developer Notes:

The project was built to learn React while providing entertainment for friends
and family.
Learning how to manage state and integrate hooks was fun and challenging.
I hope you enjoy playing the game as much as I did building it.

## Image Credits:

- [Settings](https://icons8.com/icon/FtTnPlZdnUX5/settings) icon
  by [Icons8](https://icons8.com)
- Shrek Characters and Logo
  from [Shrek Wiki](https://shrek.fandom.com/wiki/WikiShrek)
- [Shrek Background](https://pin.it/15Pm5Cq) from Pinterest